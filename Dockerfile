FROM caddy
WORKDIR /app
COPY ./app/build/web .
EXPOSE ${PORT}
CMD caddy file-server --listen :${PORT}
